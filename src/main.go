package main

import (
    "flag"
    "io"
    "io/ioutil"
    "encoding/json"
    "fmt"
    "github.com/gorilla/mux"
    "log"
    "net/http"
    "strings"
    "strconv"
    "bytes"
)

const authRootURL = "http://137.226.248.182:3000"
const reqRootURL = "http://137.226.248.182:1026"
const myPort = 1027

func main() {

    portPtr := flag.Int("port", 3000, "port the server attaches to")
    flag.Parse()

    var router = mux.NewRouter()
    // hosturl.org/interflex/api/version/

    addrArg :=  ":" + strconv.Itoa(*portPtr)

    registerHandles(router)

    fmt.Println("Running server!")
    fmt.Println("Listening on " + addrArg)

    log.Fatal(http.ListenAndServe(addrArg, router))
}

func registerHandles(router *mux.Router) {
    version := "v1"
    prefix := "/interflex/api/" + version

    fmt.Println(prefix + "/authentication/token")

    // GENERAL
    router.HandleFunc(prefix + "/healthcheck", healthCheck).Methods("GET")

    router.HandleFunc("interflex/api/version", versionCheck).Methods("GET")

    /* AUTHENTICATION */
    router.HandleFunc(prefix + "/authentication/token", handleAuthToken).Methods("POST")
    router.HandleFunc(prefix + "/authentication/refresh", handleAuthTokenRefresh).Methods("POST")

    /* DSO resources */
    /* flexibility requests */
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityRequests/requestId", handleGetRequestID).Methods("GET")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityRequests", handleGetAllFlexRequests).Methods("GET")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityRequests", handlePostFlexRequest).Methods("POST")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityRequests/{requestId}", handleDeleteFlexRequest).Methods("DELETE")


    /* flexibility activation requests */
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityActivations/activationId", handleGetActivationID).Methods("GET")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityActivations/{agg}", handleGetAllFlexActivations).Methods("GET")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityActivations/{agg}", handlePostFlexActivation).Methods("POST")
    router.HandleFunc(prefix + "/dsos/{dso}/flexibilityActivations/{agg}/{activationId}", handleDeleteFlexActivation).Methods("DELETE")


    /* AGGREGATOR resources */
    /* flexibility offers */
    router.HandleFunc(prefix + "/aggregators/flexibilityOffers", handleGetAllFlexOffers).Methods("GET")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityOffers", handlePostFlexOffer).Methods("POST")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityOffers/{offerId}", handleDeleteFlexOffer).Methods("DELETE")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityOffers/offerId", handleGetOfferID).Methods("GET")


    /* flexibility activation ACKs */
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/acks", handleGetFlexActivationAcks).Methods("GET")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/acks", handlePostFlexActivationAck).Methods("POST")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/acks/{ackId}", handleDeleteFlexActivationAck).Methods("DELETE")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/acks/ackId", handleGetAckID).Methods("GET")


    /* flexibility activation NACKs */
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/nacks", handleGetFlexActivationNacks).Methods("GET")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/nacks", handlePostFlexActivationNack).Methods("POST")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/nacks/{nackId}", handleDeleteFlexActivationNack).Methods("DELETE")
    router.HandleFunc(prefix + "/aggregators/{agg}/flexibilityActivations/nacks/nackId", handleGetNackID).Methods("GET")


}

/* ##### context update functions ##### */

func getContext(url string) []byte {
    resp, err := http.Get(url)
    if err != nil {
        log.Fatalln(err)
    }
    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatalln(err)
    }

    return body
}

func postContext(url string, data io.Reader ) {
    resp, err := http.Post(url, "application/x-www-form-urlencoded", data)
    if err != nil {
        log.Fatalln(err)
    }
    defer resp.Body.Close()
}

func putContext(url string, data io.Reader)  {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPut, url, data)
	if err != nil {
		log.Fatal(err)
    }
    
    req.Header.Set("Content-Type", "text/plain")

	_, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
    }
}

func deleteContext(url string) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, url, strings.NewReader(""))
	if err != nil {
		log.Fatal(err)
	}
	_, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
}

func requestForwarder(w *http.ResponseWriter, req *http.Request, destURL string) {
    httpClient := &http.Client{}

    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        http.Error(*w, err.Error(), http.StatusInternalServerError)
        return
    }

    req.Body = ioutil.NopCloser(bytes.NewReader(body))
    proxyReq, err := http.NewRequest(req.Method, destURL, bytes.NewReader(body))
    proxyReq.Header = req.Header

    resp, err := httpClient.Do(proxyReq)
    if err != nil {
        http.Error(*w, err.Error(), http.StatusBadGateway)
        return
    }
    defer resp.Body.Close()

    bodyBytes, err := ioutil.ReadAll(resp.Body)
    (*w).Write(bodyBytes)
}



/* ##### request handle callbacks ##### */

/* AUTHENTICATION */
func handleAuthToken(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleAuthToken")

    url := authRootURL + "/oauth2/token"
    requestForwarder(&w, req, url)
}

func handleAuthTokenRefresh(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleAuthTokenRefresh")

    url := authRootURL + "/oauth2/token"
    requestForwarder(&w, req, url)
}

/* API*/
/* flexibility requests */
func handlePostFlexRequest(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handlePostFlexRequest")

    url := reqRootURL + "/v2/entities"
    requestForwarder(&w, req, url)
}

func handleGetAllFlexRequests(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleGetAllFlexRequests")

    url := reqRootURL + "/v2/entities?type=flexibilityRequest&options=keyValues&orderBy=!dateCreated&limit=1000"
    requestForwarder(&w, req, url)
}

func handleDeleteFlexRequest(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleDeleteFlexRequest")
    fmt.Println(vars["requestId"])

    url := reqRootURL + "/v2/entities/" + vars["requestId"]
    requestForwarder(&w, req, url)
}

func handleGetRequestID(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleGetRequestID")

    url := reqRootURL + "/v2/entities/" + vars["dso"] + "/attrs/requestCount/value"
    requestCount, err := strconv.Atoi(string(getContext(url)))

    if err != nil {
        return 
    }

    requestCount++ 
    putContext(url, strings.NewReader(strconv.Itoa(requestCount)))
    requestID := vars["dso"] + "_r_" + strconv.Itoa(requestCount)
    w.Write([]byte(requestID))
}


/* flexibility activation */
func handlePostFlexActivation(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handlePostFlexActivation")
	
    url := reqRootURL + "/v2/entities"
    requestForwarder(&w, req, url)
}

func handleGetAllFlexActivations(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleGetAllFlexActivations")

    url := reqRootURL + "/v2/entities?type=flexibilityActivation&options=keyValues&orderBy=!dateCreated&limit=1000"
    requestForwarder(&w, req, url)
}

func handleDeleteFlexActivation(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleDeleteFlexActivation")
    fmt.Println(vars["activationId"])

    url := reqRootURL + "/v2/entities/" + vars["activationId"]
    requestForwarder(&w, req, url)

}

func handleGetActivationID(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleGetActivationID")

    url := reqRootURL + "/v2/entities/" + vars["dso"] + "/attrs/activationCount/value"
    activationCount, err := strconv.Atoi(string(getContext(url)))

    if err != nil {
        return 
    }

    activationCount++ 
    putContext(url, strings.NewReader(strconv.Itoa(activationCount)))
    activationID := vars["dso"] + "_a_" + strconv.Itoa(activationCount)
    w.Write([]byte(activationID))
}

/* flexibility offers */
func handlePostFlexOffer(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handlePostFlexOffer")

    url := reqRootURL + "/v2/entities"
    requestForwarder(&w, req, url)
}

func handleGetAllFlexOffers(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleGetAllFlexOffers")

    url := reqRootURL + "/v2/entities?type=flexibilityOffer&options=keyValues"
    requestForwarder(&w, req, url)
}

func handleDeleteFlexOffer(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleDeleteFlexOffer")
    fmt.Println(vars["offerId"])

    url := reqRootURL + "/v2/entities/" + vars["offerId"]
    requestForwarder(&w, req, url)
}

func handleGetOfferID(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleGetOfferID")

    url := reqRootURL + "/v2/entities/" + vars["agg"] + "/attrs/offerCount/value"
    offerCount, err := strconv.Atoi(string(getContext(url)))

    if err != nil {
        return 
    }

    offerCount++ 
    putContext(url, strings.NewReader(strconv.Itoa(offerCount)))
    offerID := vars["agg"] + "_o_" + strconv.Itoa(offerCount)
    w.Write([]byte(offerID))
}

/* flexibility activation Acks */
func handlePostFlexActivationAck(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handlePostFlexActivationAck")

    url := reqRootURL + "/v2/entities"
    requestForwarder(&w, req, url)
}

func handleGetFlexActivationAcks(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleGetFlexActivationAcks")

    url := reqRootURL + "/v2/entities?type=flexibilityActivationAck"
    requestForwarder(&w, req, url)
}

func handleDeleteFlexActivationAck(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleDeleteFlexActivationAck")
    fmt.Println(vars["ackId"])

    url := reqRootURL + "/v2/entities/" + vars["ackId"]
    requestForwarder(&w, req, url)
}

func handleGetAckID(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleGetAckID")

    url := reqRootURL + "/v2/entities/" + vars["agg"] + "/attrs/activationAckCount/value"
    activationAckCount, err := strconv.Atoi(string(getContext(url)))

    if err != nil {
        return 
    }

    activationAckCount++ 
    putContext(url, strings.NewReader(strconv.Itoa(activationAckCount)))
    ackID := vars["agg"] + "_aack_" + strconv.Itoa(activationAckCount)
    w.Write([]byte(ackID))
}

/* flexibility activation Nacks */
func handlePostFlexActivationNack(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handlePostFlexActivationNack")

    url := reqRootURL + "/v2/entities"
    requestForwarder(&w, req, url)
}

func handleGetFlexActivationNacks(w http.ResponseWriter, req *http.Request) {
    fmt.Println("@handleGetFlexActivationNacks")

    url := reqRootURL + "/v2/entities?type=flexibilityActivationNack"
    requestForwarder(&w, req, url)
}

func handleDeleteFlexActivationNack(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleDeleteFlexActivationNack")
    fmt.Println(vars["nackId"])

    url := reqRootURL + "/v2/entities/" + vars["nackId"]
    requestForwarder(&w, req, url)
}

func handleGetNackID(w http.ResponseWriter, req *http.Request) {
    vars := mux.Vars(req)
    fmt.Println("@handleGetNackID")

    url := reqRootURL + "/v2/entities/" + vars["agg"] + "/attrs/activationNackCount/value"
    activationNackCount, err := strconv.Atoi(string(getContext(url)))

    if err != nil {
        log.Println(err)
        return 
    }

    activationNackCount++ 
    putContext(url, strings.NewReader(strconv.Itoa(activationNackCount)))
    nackID := vars["agg"] + "_anack_" + strconv.Itoa(activationNackCount)
    w.Write([]byte(nackID))
}


/* others */
func handleUrlDsos(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    dso := vars["dso"]

    data := getContext(reqRootURL + "/version")

    fmt.Println(dso)
    fmt.Println(string(data))

    json.NewEncoder(w).Encode(dso)
}

func handleUrlAggregators(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    agg := vars["agg"]

    data := getContext(reqRootURL + "/version")

    fmt.Println(agg)
    fmt.Println(string(data))

    json.NewEncoder(w).Encode(agg)
}

/* GENERAL */
func healthCheck(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    type Health struct {
        Status string
        Uptime string
        //TODO
    }
    h := Health {"alive", "unknown"}
    json.NewEncoder(w).Encode(h)
}

func versionCheck(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    type Version struct {
        ApiVersion string
        PlatfomVersion string
        //TODO
    }
    v := Version {"v1", "v0.0.1"}
    json.NewEncoder(w).Encode(v)
}
